"use strict";
$(function(){

    checkoutForm();

    calculatorSync();
});

var checkoutForm = function () {
    var els = $('.vk-form--checkout input, .vk-form--checkout textarea');

    function telIsNumberOnly(){
        var el = $("#tel");
        el.keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

    function activeListHandleClick() {
        var el = $('[data-list="active"]');
        // console.log(this);
        el.on('click', 'li', function () {
            $(this).siblings().removeClass('active');

            if ($(this).hasClass('active')) {
                $(this).toggleClass('active');
            }else{
                $(this).addClass('active');
            }



        })
    }

    function inputHandleFocus() {
        els.on('focus', function () {
            $(this).addClass('active');
        })
    }

    function inputHandleBlur() {
        els.on('blur', function () {
            var el = $(this);
            // console.log(el.val().trim().length);
            if (el.val().trim().length < 1) {
                $(this).removeClass('active');
            }


        })
    }

    function checkoutFormValidation(){
        var el =  $("#checkoutForm");

        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "Value must not equal arg.");

        el.validate({

            rules: {
                fullname: {
                    required: true,
                    minlength: 2,
                },
                tel: {
                    required: true,
                    minlength: 10,
                    maxlength: 11,
                },
                province: {
                    required: true,
                    valueNotEquals: '0'
                },
                district: {
                    required: true,
                    valueNotEquals: '0'
                },
                addr: {
                    required: true,
                }
            },

            //noti
            messages: {
                fullname: {
                    required: "Nhập họ tên của bạn",
                    minlength: "Họ tên của bạn quá ngắn"
                },
                tel: {
                    required: "Nhập số điện thoại của bạn",
                    minlength: "Số điện thoại không đúng định dạng 10 hoặc 11 số",
                    maxlength: "Số điện thoại không đúng định dạng 10 hoặc 11 số",
                },
                province: {
                    required: "Chọn tỉnh/thành",
                    valueNotEquals: 'Chọn tỉnh/thành'
                },
                district: {
                    required: "Chọn quận/huyện",
                    valueNotEquals: 'Chọn quận/huyện'
                },
                addr: {
                    required: "Nhập địa nhận hàng",
                }
            }
        });
    }

    function run() {
        telIsNumberOnly();
        inputHandleFocus();
        inputHandleBlur();
        activeListHandleClick();
        checkoutFormValidation();
    }

    run();

};

var calculatorSync = function () {

    function handleClick() {
        $("[data-calculator-cart] .vk-btn").on("click", function () {


            var button = $(this);

            var parent = button.closest('tr');

            var oldValue = button.siblings("input").val();

            var newVal;

            if (button.attr('data-index') === "plus") {

                if (oldValue > 0) {
                    newVal = parseFloat(oldValue) + 1;
                } else {
                    newVal = 1;
                }

            } else {
                // Don't allow decrementing below zero
                if (oldValue > 1) {
                    newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 1;
                }
            }

            button.siblings("input").val(newVal);

            //cal sync

            calTotal(parent, newVal);

            return false;
        });
    }

    function handChange() {
        $('.vk-calculator input').on('keyup', function (e) {
            var keyCode = e.keyCode;

            var parent = $(this).closest('tr');
            var newVal = $(this).val();

            if (newVal.length === 0) {
                $(this).val(0);
            }
            calTotal(parent, newVal);
            if (keyCode === 8) {
                calTotal(parent, newVal);
            }

            if (keyCode === 43 || keyCode === 45) {
                return false;
            }
        })
    }

    function init() {
        var rowDataFirst = $('.vk-table--cart tbody tr:first');

        if (rowDataFirst.length) {
            var newVal = rowDataFirst.find('.vk-calculator input').val();
            newVal = parseFloat(newVal);

            calTotal(rowDataFirst, newVal);
        }
    }

    function calTotal(parent, newVal) {
        var price = parent.find('.vk-shopcart-item__price').data('price');
        var priceTotal = parent.find('.vk-shopcart-item__price--total');
        // console.log(price);

        var result = price * newVal;


        var resultPrime = result;

        parent.siblings().each(function () {
            var priceSibling = $(this).find('.vk-shopcart-item__price').data('price');
            var quantitySibling = $(this).find('.vk-calculator input').val();
            // console.log(priceSibling);

            priceSibling = parseFloat(priceSibling);
            quantitySibling = parseFloat(quantitySibling);

            resultPrime += priceSibling * quantitySibling;


        });

        priceTotal.text(numeral(result).format('0,0'));
        $('#shopcartPriceTotal').text(numeral(resultPrime).format('0,0'))


    }

    function cartRowDataDelete() {
        $('.vk-shopcart-item__btn-del').on('click', function (e) {
            e.preventDefault();

            var parent = $(this).closest('tr');
            calTotal(parent, 0)
            parent.remove();

            return false;
        })
    };

    function run() {
        init();
        handleClick();
        handChange();
        cartRowDataDelete();

    }

    run();
}
