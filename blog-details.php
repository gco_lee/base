<?php
include 'classes/classes.php';

//html
$head->render('Tin tức chi tiết');
$header->class_header = '';
$header->render();

$breadcrumb->render(['Tin tức chi tiết',['blog.php','Tin tức']]);
?>
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <h1 class="vk-blog-detail__heading">Tiêu đề mẫu</h1>
                <div class="vk-blog-item__date vk-blog-item__date--detail">Thứ Hai, 14/11/2016 00:13</div>

                <div class="vk-blog-detail__content">

                    <h5>Đoạn văn mẫu</h5>
                    <p>mẫu</p>

                </div>

                <div class="vk-blog__relate">
                    <h2 class="vk-blog__title">Tin liên quan</h2>

                    <div class="vk-blog__list row">
                        <div class="col-sm-6 col-md-12 _item">
                            <div class="vk-blog-item vk-blog-item--style-1">
                                <a href="blog-details.php" title="Tiêu đề mẫu" class="vk-blog-item__img">
                                    <img src="images/blog-1.jpg" alt="Tiêu đề mẫu" class="_img">
                                </a>

                                <div class="vk-blog-item__brief">
                                    <h3 class="vk-blog-item__title"><a href="blog-details.php" title="Tiêu đề mẫu">Tiêu đề mẫu</a></h3>
                                    <div class="vk-blog-item__date">Thứ Hai, 14/11/2016 00:13</div>
                                    <div class="vk-blog-item__text" data-truncate-lines="3">Nội dung mô tả ngắn mẫu gôm vài dòng viết linh tinh để làm mẫu, phía trên là ảnh mẫu và tiêu đề mẫu ngắn gọn với mục đích chỉ để làm cảnh và đoạn này chỉ hiển thị được ba dòng...</div>
                                    <a href="blog-details.php" class="vk-blog-item__btn">Xem thêm <i class="_icon ti-arrow-right"></i></a>
                                </div>
                            </div> <!--./vk-blog-item-->
                        </div>
                        <div class="col-sm-6 col-md-12 _item">
                            <div class="vk-blog-item vk-blog-item--style-1">
                                <a href="blog-details.php" title="Tiêu đề mẫu" class="vk-blog-item__img">
                                    <img src="images/blog-1.jpg" alt="Tiêu đề mẫu" class="_img">
                                </a>

                                <div class="vk-blog-item__brief">
                                    <h3 class="vk-blog-item__title"><a href="blog-details.php" title="Tiêu đề mẫu">Tiêu đề mẫu</a></h3>
                                    <div class="vk-blog-item__date">Thứ Hai, 14/11/2016 00:13</div>
                                    <div class="vk-blog-item__text" data-truncate-lines="3">Nội dung mô tả ngắn mẫu gôm vài dòng viết linh tinh để làm mẫu, phía trên là ảnh mẫu và tiêu đề mẫu ngắn gọn với mục đích chỉ để làm cảnh và đoạn này chỉ hiển thị được ba dòng...</div>
                                    <a href="blog-details.php" class="vk-blog-item__btn">Xem thêm <i class="_icon ti-arrow-right"></i></a>
                                </div>
                            </div> <!--./vk-blog-item-->
                        </div>
                    </div>
                </div>

                <div class="vk-comment pt-50">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="2"></div>
                </div>

            </div> <!--./col-->

            <div class="col-lg-3 pt-50 pt-lg-0">
                <div class="vk-sidebar">
                    <div class="vk-sidebar__box">
                        <h2 class="vk-sidebar__title">Tin mới nhất</h2>

                        <ul class="vk-sidebar__list vk-sidebar__list--blog">
                            <li><a href="#">Tiêu đề mẫu</a></li>
                            <li><a href="#">Tiêu đề mẫu</a></li>
                            <li><a href="#">Tiêu đề mẫu</a></li>
                            <li><a href="#">Tiêu đề mẫu</a></li>
                            <li><a href="#">Tiêu đề mẫu</a></li>
                        </ul>
                    </div>
                </div> <!--./sidebar-->
            </div> <!--./col-->
        </div> <!--./row-->



    </div> <!--./container-->

<?php
//Footer
$footer->render();


//srcipt
include 'template/modules/end.temp.php';


