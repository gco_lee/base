	</div>
    <!--./main-wrapper-->

    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        else window.addEventListener('load', loadDeferredStyles);
    </script>
    
    <!-- BEGIN: SCRIPT -->
    <script src="plugin/jquery/jquery-3.2.1.min.js" defer ></script>
    <script src="plugin/jquery/jquery-migrate-3.0.0.min.js" defer ></script>
    <script src="plugin/popper/popper.min.js" defer ></script>
    <script src="plugin/bootstrap/js/bootstrap.min.js" defer ></script>
    <script src="plugin/mmenu/jquery.mmenu.all.min.js" defer ></script>
    <script src="plugin/OwlCarousel/owl.carousel.min.js" defer ></script>
    <script src="plugin/OwlCarousel/owl.carousel.sync.min.js" defer ></script>
    <script src="plugin/scrollup/jquery.scrollUp.min.js" defer ></script>
    <script src="plugin/jquery-nice-select/js/jquery.nice-select.js" defer ></script>
    <script src="plugin/magiczoomplus/magiczoomplus.min.js" defer ></script>
    <script src="plugin/scrollbar/jquery.scrollbar.min.js" defer ></script>
    <script src="plugin/animsition/animsition.min.js" defer></script>

    <script src="plugin/jquery-ui/jquery-ui.min.js" defer ></script>
    <script src="plugin/numeral/numeral.min.js" defer ></script>
    <script src="plugin/fancybox/jquery.fancybox.min.js" defer ></script>
    <script src="plugin/stickOnScroll/jquery.stickOnScroll.min.js" defer ></script>
    <script src="plugin/jquery-validation/jquery.validate.min.js" defer ></script>

    <script src="plugin/animate/animate.min.js" defer ></script>
    <script src="plugin/waypoint/jquery.waypoints.min.js" defer ></script>
    <script src="plugin/truncate/truncate.min.js" defer ></script>
    <script src="plugin/isotope/isotope.pkgd.min.js" defer ></script>
    <script src="plugin/masonry/masonry.pkgd.min.js" defer ></script>


    <script src="plugin/countTo/jquery.countTo.min.js" defer ></script>


    <script src="plugin/main.js" defer ></script>
	<!-- END: SCRIPT -->
</body>
</html>


