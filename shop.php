<?php
include 'classes/classes.php';

//html
$head->render('Shop');
$header->class_header = '';
$header->render();

$breadcrumb->render(['Sản phẩm']);
?>


    <div class="container">
        <div class="row">
            <div class="col-lg-3 order-1 order-lg-0 pt-50 pt-lg-0">

                <div class="vk-sidebar">

                    <div class="vk-sidebar__box">
                        <h2 class="vk-sidebar__title">Tìm kiếm sản phẩm</h2>

                        <div class="vk-sidebar__search">
                            <div class="vk-form vk-form--search">
                                <input type="text" class="form-control" placeholder="Từ khóa tìm kiếm">
                                <button class="vk-btn "><i class="ti-search"></i></button>
                            </div>
                        </div>
                    </div> <!--./box-->

                    <div class="vk-sidebar__box">
                        <h2 class="vk-sidebar__title">Danh mục</h2>
                        <ul class="vk-sidebar__list vk-sidebar__list--shop">
                            <li><a href="#">Tiêu đề mẫu</a></li>
                            <li><a href="#">Tiêu đề mẫu</a></li>
                            <li><a href="#">Tiêu đề mẫu</a></li>
                            <li><a href="#">Tiêu đề mẫu</a></li>
                            <li><a href="#">Tiêu đề mẫu</a></li>
                        </ul>
                    </div> <!--./box-->

                    <div class="vk-sidebar__box">
                        <h2 class="vk-sidebar__title">Lọc giá</h2>

                        <div class="vk-range">
                            <div id="slider-range" class="vk-range__content" data-min="0" data-max="5000000"></div>

                            <input type="hidden" id="amount1">
                            <input type="hidden" id="amount2">

                            <div class="vk-range__show">
                                <div class="_from"><span id="text_amount1"></span>VNĐ</div>
                                <div class="_to"><span id="text_amount2"></span>VNĐ</div>
                            </div>
                        </div>
                    </div> <!--./box-->







                </div> <!--./sidebar-->

            </div> <!--./col-->

            <div class="col-lg-9 order-0 order-lg-1">
                <div class="vk-banner vk-banner--shop">
                    <img src="" alt="">
                </div> <!--./banner-->

                <div class="vk-shop__list row">
                    <?php $listItem->shop_item(); ?>
                </div>

                <?php $pagination->render(); ?>
            </div> <!--./col-->
        </div>
    </div>


<?php
//Footer
$footer->render();


//srcipt
include 'template/modules/end.temp.php';


