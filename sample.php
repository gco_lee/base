<?php
include 'classes/classes.php';

//html
$head->render('Home');
$header->class_header = '';
$header->render();
?>
<!--hotline-->
    <a href="tel:0911077422" class="fancybox">
        <div class="coccoc-alo-phone coccoc-alo-green coccoc-alo-show" id="coccoc-alo-phoneIcon">
            <div class="coccoc-alo-ph-circle"></div>
            <div class="coccoc-alo-ph-circle-fill"></div>
            <div class="coccoc-alo-ph-img-circle"></div>
        </div>
    </a>


    <div class="container pb-50">
        <div class="row">
            <div class="col-lg-6 pt-50">
                <h3>Magic zoom plus</h3>
                <!--BEGIN: MAGIC ZOOM PLUS-->
                <!-- Main zoom image -->
                <a class="MagicZoom" id="zoom"
                   data-options="textHoverZoomHint: Hover to zoom1; textExpandHint: Click to expand1"
                   title="..." href="https://source.unsplash.com/juHayWuaaoQ/1500x1000"><img src="https://source.unsplash.com/juHayWuaaoQ/1500x1000"/></a>

                <!-- Thumbnails -->
                <div class="row no-gutters">
                    <div class="col-4"><a data-zoom-id="zoom" href="https://source.unsplash.com/juHayWuaaoQ/1500x1000" data-image="https://source.unsplash.com/juHayWuaaoQ/1500x1000" title="..."><img src="https://source.unsplash.com/juHayWuaaoQ/240x160"/></a></div>
                    <div class="col-4"><a data-zoom-id="zoom" href="https://source.unsplash.com/eWFdaPRFjwE/1500x1000" data-image="https://source.unsplash.com/eWFdaPRFjwE/1500x1000" title="..."><img src="https://source.unsplash.com/eWFdaPRFjwE/240x160"/></a></div>
                    <div class="col-4"><a data-zoom-id="zoom" href="https://source.unsplash.com/i2KibvLYjqk/1500x1000" data-image="https://source.unsplash.com/i2KibvLYjqk/1500x1000" title="..."><img src="https://source.unsplash.com/i2KibvLYjqk/240x160"/></a></div>
                </div>

                <!--END: MAGIC ZOOM PLUS-->
            </div> <!--./col-->
            <div class="col-lg-6 pt-50">
                <h3>Image gallery</h3>
                <p>
                    Groups are created by adding the same <code>data-fancybox</code> attribute value
                </p>

                <!--BEGIN: FANCYBOX-->
                <p class="imglist" style="max-width: 1000px;">
                    <a href="https://source.unsplash.com/juHayWuaaoQ/1500x1000" data-fancybox="images">
                        <img src="https://source.unsplash.com/juHayWuaaoQ/240x160"/>
                    </a>

                    <a href="https://source.unsplash.com/eWFdaPRFjwE/1500x1000" data-fancybox="images">
                        <img src="https://source.unsplash.com/eWFdaPRFjwE/240x160"/>
                    </a>

                    <a href="https://source.unsplash.com/i2KibvLYjqk/1500x1000" data-fancybox="images">
                        <img src="https://source.unsplash.com/i2KibvLYjqk/240x160"/>
                    </a>

                    <a href="https://source.unsplash.com/RFgO9B_OR4g/1500x1000" data-fancybox="images">
                        <img src="https://source.unsplash.com/RFgO9B_OR4g/240x160"/>
                    </a>

                    <a href="https://source.unsplash.com/7bwQXzbF6KE/1500x1000" data-fancybox="images">
                        <img src="https://source.unsplash.com/7bwQXzbF6KE/240x160"/>
                    </a>

                    <a href="https://source.unsplash.com/NhU0nUR7920/1500x1000" data-fancybox="images">
                        <img src="https://source.unsplash.com/NhU0nUR7920/240x160"/>
                    </a>
                </p>
                <!--END: FANCYBOX-->
            </div> <!--./col-->


            <div class="col-lg-6 pt-50">
                <h3>Truncate line</h3>

                <!--BEGIN: TRUNCATE LINES-->
                <div data-truncate-lines="4" style="max-width: 500px;">
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                        and
                        scrambled it to make a type specimen book. It has survived not only five centuries, but also the
                        leap into
                        electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                        release of
                        Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                        like
                        Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                </div>
                <!--END: TRUNCATE LINES-->
            </div> <!--./col-->

            <div class="col-lg-6 pt-50">

                <h3>Nice select</h3>

                <!--BEGIN:NICE SELECT-->
                <select class="form-control wide" data-nice-select>
                    <option value="">option 1</option>
                    <option value="">option 2</option>
                    <option value="">option 3</option>
                    <option value="">option 4</option>
                </select>
                <!--END:NICE SELECT-->

            </div> <!--./col-->

            <div class="col-lg-6 pt-50">
                <h3>Calculator</h3>

                <!--BEGIN: CALCULATOR-->
                <div class="vk-calculator" data-calculator>
                    <input type="number" name="name" value="1" min="1" class="form-control">
                    <a href="#" class="vk-btn _plus" data-index="plus">+</a>
                    <a href="#" class="vk-btn _minus" data-index="minus">-</a>
                </div> <!--./calculator-->
                <!--END: CALCULATOR-->

            </div> <!--./col-->


            <div class="col-lg-6 pt-50">
                <h3>Range slider Price</h3>

                <!--BEGIN: RANGE SLIDER-->
                <div class="vk-range">
                    <div id="slider-range" class="vk-range__content" data-min="0" data-max="5000000"></div>

                    <input type="hidden" id="amount1">
                    <input type="hidden" id="amount2">

                    <div class="vk-range__show">
                        <div class="_from"><span id="text_amount1"></span>vnđ</div>
                        <div class="_to"><span id="text_amount2"></span>vnđ</div>
                    </div>
                </div>
                <!--END: RANGE SLIDER-->
            </div> <!--./col-->

            <div class="col-lg-6 pt-50">
                <h3>Checkbox radio</h3>
                <!--BEGIN: CHECKBOX RADIO-->

                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">Check this custom checkbox</label>
                </div>

                <!--RADIO-->
                <div class="custom-control custom-radio">
                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                    <label class="custom-control-label" for="customRadio1">Toggle this custom radio</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                    <label class="custom-control-label" for="customRadio2">Or toggle this other custom radio</label>
                </div>

                <!--END: CHECKBOX RADIO-->
            </div> <!--./col-->

            <div class="col-lg-6 pt-50">
                <h3>Accordion</h3>
                <!--BEGIN: ACCORDION-->
                <div class="accordion accordion--style-2" id="accordionExample">
                    <div class="card">
                        <div class="card-header">
                            <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                Collapsible Group Item #1
                            </a>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" >

                            <a class="card-link collapsed" data-toggle="collapse" href="#collapseTwo">
                                Collapsible Group Item #2
                            </a>

                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" >

                            <a class="card-link collapsed" data-toggle="collapse" href="#collapseThree">
                                Collapsible Group Item #3
                            </a>

                        </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordionExample">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
                <!--END: ACCORDION-->
            </div> <!--./col-->


            <div class="col-lg-6 pt-50">
                <h3>Nav horizontal</h3>
                <div class="nav--style-1">
                    <ul class="nav">
                        <li class="nav__item"><a class="active" data-toggle="tab" href="#home">Home</a></li>
                        <li class="nav__item"><a class="" data-toggle="tab" href="#profile">Profile</a></li>
                        <li class="nav__item"><a class="" data-toggle="tab" href="#contact">Contact</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="home" role="tabpanel">Home</div>
                        <div class="tab-pane fade" id="profile" role="tabpanel">Profile</div>
                        <div class="tab-pane fade" id="contact" role="tabpanel">Contact</div>
                    </div>
                </div>
            </div> <!--./col-->

            <div class="col-lg-6 pt-50">
                <h3>Nav vertical</h3>
                <div class="nav--style-2">
                    <ul class="nav">
                        <li class="nav__item"><a class="active" data-toggle="tab" href="#home1">Home</a></li>
                        <li class="nav__item"><a class="" data-toggle="tab" href="#profile1">Profile</a></li>
                        <li class="nav__item"><a class="" data-toggle="tab" href="#contact1">Contact</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="home1" role="tabpanel">Home</div>
                        <div class="tab-pane fade" id="profile1" role="tabpanel">Profile</div>
                        <div class="tab-pane fade" id="contact1" role="tabpanel">Contact</div>
                    </div>
                </div>
            </div> <!--./col-->
            <div class="col-lg-6 pt-50">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Launch demo modal
                </button>

                <!-- Modal -->
                <div class="modal fade vk-modal" id="exampleModal">
                    <div class="modal-dialog vk-modal__dialog" role="document">
                        <div class="modal-content vk-modal__content">
                            <div class="modal-header vk-modal__header">
                                <h5 class="modal-title vk-modal__title">Modal title</h5>
                                <button type="button" class="close vk-modal__btn-close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body vk-modal__body">
                                content
                            </div>
                            <div class="modal-footer vk-modal__footer">
                                <button type="button" class="vk-btn vk-btn--secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="vk-btn vk-btn--primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--./col-->
            <div class="col-lg-6">
                <div class="vk-form--validation">
                    <h3>Đăng ký tài khoản</h3>
                    <form  id="signupForm" method="get" action="">

                        <div class="form-group">
                            <label for="firstname">Họ tên</label>
                            <input id="firstname" name="firstname" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="test">test</label>
                            <input id="test" name="test" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" name="email" type="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="tel">Số điện thoại</label>
                            <input id="tel" name="tel" type="tel" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input id="username" name="username" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Mật khẩu</label>
                            <input id="password" name="password" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Nhập lại mật khẩu</label>
                            <input id="confirm_password" name="confirm_password" type="password" class="form-control">
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="agree" name="agree">
                                <label class="custom-control-label" for="agree">Đồng ý với điều khoản</label>
                            </div>

                        </div>


                        <div class="form-group">
                            <input class="btn btn-success" type="submit" value="Đăng ký" >
                        </div>

                    </form>
                </div>
            </div>

        </div> <!--./row-->








        </div>
    </div>

<?php
//Footer
$footer->render();


//srcipt
include 'template/modules/end.temp.php';


