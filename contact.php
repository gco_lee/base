<?php
include 'classes/classes.php';

//html
$head->render('Liên hệ');
$header->class_header = '';
$header->render();

$breadcrumb->render(['Liên hệ']);
?>
    <div class="container">
        <h1 class="vk-contact__heading">Liên hệ</h1>

       <div class="row">
           <div class="col-lg-8">
               <h2 class="vk-contact__title">Gửi tin liên hệ</h2>

               <div class="vk-contact__form">
                   <div class="vk-form vk-form--contact">
                       <div class="form-group">
                           <input type="text" class="form-control" placeholder="Họ tên">
                       </div> <!--./form-group-->

                       <div class="row">
                           <div class="col-lg-6">
                               <div class="form-group">
                                   <input type="text" class="form-control" placeholder="Email">
                               </div>
                           </div>
                           <div class="col-lg-6">
                               <div class="form-group">
                                   <input type="text" class="form-control" placeholder="Số điện thoại">
                               </div>
                           </div>
                       </div> <!--./row-->


                       <div class="form-group">
                           <input type="text" class="form-control" placeholder="Chủ đề">
                       </div> <!--./form-group-->

                       <div class="form-group">
                           <textarea class="form-control" placeholder="Nội dung"></textarea>
                       </div> <!--./form-group-->

                       <div class="text-right">
                           <button class="vk-btn vk-btn--pink-1">Gửi</button>
                       </div>

                   </div>
               </div> <!--./form-->

           </div> <!--./col-->

           <div class="col-lg-4 pt-50 pt-lg-0">
               <h2 class="vk-contact__title">Thông tin liên hệ</h2>
               <ul class="vk-contact__list">
                   <li><i class="_icon fa fa-map-marker"></i> <b>Địa chỉ:</b> Tầng 8, Tòa nhà TOYOTA Thanh Xuân, 315 Trường Chinh, Thanh Xuân, Hà Nội</li>
                   <li><i class="_icon fa fa-phone"></i> <b>Hotline:</b> <a href="tel:(023)7 309 885">(023)7 309 885</a></li>
                   <li><i class="_icon fa fa-envelope"></i> <b>Email:</b> <a href="mailto:info@gco.vn">info@gco.vn</a></li>
               </ul>
           </div> <!--./col-->
       </div> <!--./row-->


    </div> <!--./container-->

    <div class="vk-contact__map pt-50">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.77497474661!2d105.82069491473096!3d21.001655486013075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac83e47ecaa9%3A0x110275dc966aadd8!2sToyota+Thanh+Xuan!5e0!3m2!1sen!2s!4v1534414076316" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

<?php
//Footer
$footer->render();


//srcipt
include 'template/modules/end.temp.php';


