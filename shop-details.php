<?php
include 'classes/classes.php';

//html
$head->render('Shop');
$header->class_header = '';
$header->render();

$breadcrumb->render(['Sản phẩm chi tiết', ['shop.php', 'Sản phẩm']]);
?>


    <div class="container">
        <div class="vk-shop-detail__top">
            <div class="row">
                <div class="col-lg-6">
                    <div class="vk-shop-detail__thumbnail">

                        <div class="vk-shop-detail__for">
                            <a class="MagicZoom" id="zoom"
                               data-options="textHoverZoomHint: Di qua để phóng to; textExpandHint: Click để xem chi tiết"
                               title="..." href="https://source.unsplash.com/juHayWuaaoQ/1500x1000"><img
                                        src="https://source.unsplash.com/juHayWuaaoQ/1500x1000"/></a>

                        </div>


                        <!-- Thumbnails -->
                        <div class="vk-shop-detail__nav">
                            <div class="vk-slider--style-1 owl-carousel" data-slider="slider-nav">
                                <div class="_item">
                                    <div class="vk-sd-nav-item">
                                        <a data-zoom-id="zoom"
                                           href="https://source.unsplash.com/juHayWuaaoQ/1500x1000"
                                           data-image="https://source.unsplash.com/juHayWuaaoQ/1500x1000"
                                           title="..."><img
                                                    src="https://source.unsplash.com/juHayWuaaoQ/240x160"/></a>
                                    </div>

                                </div>
                                <div class="_item">
                                    <div class="vk-sd-nav-item">
                                        <a data-zoom-id="zoom"
                                           href="https://source.unsplash.com/eWFdaPRFjwE/1500x1000"
                                           data-image="https://source.unsplash.com/eWFdaPRFjwE/1500x1000"
                                           title="..."><img
                                                    src="https://source.unsplash.com/eWFdaPRFjwE/240x160"/></a>
                                    </div>

                                </div>
                                <div class="_item">
                                    <div class="vk-sd-nav-item">
                                        <a data-zoom-id="zoom"
                                           href="https://source.unsplash.com/i2KibvLYjqk/1500x1000"
                                           data-image="https://source.unsplash.com/i2KibvLYjqk/1500x1000"
                                           title="..."><img
                                                    src="https://source.unsplash.com/i2KibvLYjqk/240x160"/></a>
                                    </div>

                                </div>

                                <div class="_item">
                                    <div class="vk-sd-nav-item">
                                        <a data-zoom-id="zoom"
                                           href="https://source.unsplash.com/juHayWuaaoQ/1500x1000"
                                           data-image="https://source.unsplash.com/juHayWuaaoQ/1500x1000"
                                           title="..."><img
                                                    src="https://source.unsplash.com/juHayWuaaoQ/240x160"/></a>
                                    </div>

                                </div>
                                <div class="_item">
                                    <div class="vk-sd-nav-item">
                                        <a data-zoom-id="zoom"
                                           href="https://source.unsplash.com/eWFdaPRFjwE/1500x1000"
                                           data-image="https://source.unsplash.com/eWFdaPRFjwE/1500x1000"
                                           title="..."><img
                                                    src="https://source.unsplash.com/eWFdaPRFjwE/240x160"/></a>
                                    </div>

                                </div>
                                <div class="_item">
                                    <div class="vk-sd-nav-item">
                                        <a data-zoom-id="zoom"
                                           href="https://source.unsplash.com/i2KibvLYjqk/1500x1000"
                                           data-image="https://source.unsplash.com/i2KibvLYjqk/1500x1000"
                                           title="..."><img
                                                    src="https://source.unsplash.com/i2KibvLYjqk/240x160"/></a>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-lg-6 pt-lg-0 pt-50">
                    <h1 class="vk-shop-detail__heading">Tiêu đề mẫu</h1>
                    <div class="vk-shop-detail__price">1 600 000 VNĐ</div>
                    <div class="vk-shop-detail__short-content">
                        <p>
                            Mô tả ngắn
                        </p>
                    </div>

                    <div class="vk-shop-detail__qty">
                        <label for="qtyField" class=""> Số lượng:</label>

                        <div class="vk-calculator" data-calculator>
                            <input id="qtyField" type="number" name="name" value="1" min="1" class="form-control">
                            <a href="#" class="vk-btn _plus" data-index="plus">+</a>
                            <a href="#" class="vk-btn _minus" data-index="minus">-</a>
                        </div> <!--./calculator-->

                    </div>

                    <div class="vk-shop-detail__button">
                        <a href="#" class="vk-shop-detail__btn-addtocart">Thêm vào giỏ</a>
                    </div>
                </div>
            </div> <!--./row-->
        </div> <!--./top-->

        <div class="vk-shop-detail__bot pt-50">
            <div class="vk-shop-detail__content">
                <h2>Tiêu đề mẫu</h2>
                <p>Đoạn văn mẫu</p>
            </div>

            <div class="vk-comment">
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1';
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="2"></div>
            </div>
        </div> <!--./bot-->

        <div class="vk-shop__relate pt-50">
            <h2 class="vk-page__title">Sản phẩm liên quan</h2>
            <div class="row owl-carousel vk-slider--style-1" data-slider="relate">
                <?php
                $listItem->shopItem__wrapper = 'col-12';
                $listItem->shop_item();
                ?>
            </div>
        </div>

    </div> <!--./container-->


<?php
//Footer
$footer->render();


//srcipt
include 'template/modules/end.temp.php';


