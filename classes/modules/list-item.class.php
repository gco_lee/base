<?php
// STEP 2 - CREATE CLASS (IF NOT HAVE) IN classes dir 
// IF HAVED NEXT TO STEP 3
// JUST COPY AND EDIT PLACE WITH * SIGN

// List of buttons class
class ListItem
{

    public $el;
    public $temp = 'list-item.temp.html';            // edit temp name here *
    public $path = 'template/modules';            // edit path name here *

    public function __construct()
    {
        $this->shopItem__wrapper = 'col-sm-6 col-md-3 col-lg-3';
        $this->shopItem__link = 'shop-details.php';
        $this->shopItem__data = array(
            ['shop-1.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-2.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-3.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-4.jpg','Tiêu đề mẫu','1.000.000'],

            ['shop-5.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-6.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-7.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-8.jpg','Tiêu đề mẫu','1.000.000'],

            ['shop-9.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-10.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-11.jpg','Tiêu đề mẫu','1.000.000'],
            ['shop-12.jpg','Tiêu đề mẫu','1.000.000'],
        );

        $content = 'Nội dung mô tả ngắn mẫu gôm vài dòng viết linh tinh để làm mẫu, phía trên là ảnh mẫu và tiêu đề mẫu ngắn gọn với mục đích chỉ để làm cảnh và đoạn này chỉ hiển thị được ba dòng...';
        $this->blogItem__wrapper = 'col-sm-6 col-md-3 col-lg-4';
        $this->blogItem__link = 'blog-details.php';
        $this->blogItem__data = array(
            ['blog-1.jpg','Tiêu đề mẫu',$content],
            ['blog-2.jpg','Tiêu đề mẫu',$content],
            ['blog-3.jpg','Tiêu đề mẫu',$content],

            ['blog-4.jpg','Tiêu đề mẫu',$content],
            ['blog-5.jpg','Tiêu đề mẫu',$content],
            ['blog-6.jpg','Tiêu đề mẫu',$content],

            ['blog-7.jpg','Tiêu đề mẫu',$content],
            ['blog-8.jpg','Tiêu đề mẫu',$content],
            ['blog-9.jpg','Tiêu đề mẫu',$content],


        );
    }


    public function shop_item($class='')
    {

        $this->el = new XTemplate($this->temp, $this->path);
        $this->el->assign('WRAPPER', $this->shopItem__wrapper);
        $this->el->assign('LINK', $this->shopItem__link);
        $this->el->assign('CLASS', $class);


            foreach ($this->shopItem__data as $value) {

                $this->el->assign('IMG', $value[0]);
                $this->el->assign('TITLE', $value[1]);
                $this->el->assign('PRICE', $value[2]);

                $this->el->parse('shop_item');
            }


        echo $this->el->text('shop_item');
    }
    
    public function blog_item($class='')
    {

        $this->el = new XTemplate($this->temp, $this->path);
        $this->el->assign('WRAPPER', $this->blogItem__wrapper);
        $this->el->assign('LINK', $this->blogItem__link);
        $this->el->assign('CLASS', $class);


            foreach ($this->blogItem__data as $value) {

                $this->el->assign('IMG', $value[0]);
                $this->el->assign('TITLE', $value[1]);
                $this->el->assign('TEXT', $value[2]);

                $this->el->parse('blog_item');
            }


        echo $this->el->text('blog_item');
    }

}
