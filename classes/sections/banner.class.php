<?php
// STEP 2 - CREATE CLASS (IF NOT HAVE) IN classes dir 
// IF HAVED NEXT TO STEP 3
// JUST COPY AND EDIT PLACE WITH * SIGN

// List of buttons class
class Banner
{

    public $el;
    public $temp = 'banner.temp.html';            // edit temp name here *
    public $path = 'template/sections';            // edit path name here *

    public function render($args = array())
    {

        $this->el = new XTemplate($img='banner-1.jpg',$class='vk-banner--style-1');


        $this->el->assign('IMG',$img);
        $this->el->assign('CLASS',$class);

        $this->el->parse('main');
        echo $this->el->text('main');

    }
}
