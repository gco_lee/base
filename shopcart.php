<?php
include 'classes/classes.php';

//html
$head->render('shopcart');
$header->class_header = '';
$header->render();
$breadcrumb->render(['Giỏ hàng'])
?>
<div class="vk-shopcart pt-50 pb-50">
    <div class="container">
        <h1 class="vk-shopcart__title">Giỏ hàng của bạn ( 2 sản phẩm )</h1>
        <div class="table-responsive">
            <table class="table vk-table--cart">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Sản phẩm</th>
                    <th>Giá</th>
                    <th>Số lượng</th>
                    <th>Tổng</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>
                        <div class="vk-shopcart-item">
                            <a href="#" class="vk-shopcart-item__img">
                                <img src="images/cart-1.jpg" alt="">
                            </a>
                            <div class="vk-shopcart-item__brief">
                                <h3 class="vk-shopcart-item__title"><a href="#">Tiêu đề mẫu</a></h3>
                                <div class="vk-shopcart-item__text">
                                    <p>Màu sắc: <span class="_color"><img src="" alt=""></span></p>
                                    <p>Size: <span class="badge badge-secondary">XS</span></p>
                                </div>
                                <a href="#" class="vk-shopcart-item__btn-del"><i class="_icon fa fa-trash"></i> Xóa sản phẩm</a>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="vk-shopcart-item__price" data-price="1000000">1.000.000 đ</div>
                    </td>
                    <td>
                        <div class="vk-calculator" data-calculator-cart>
                            <input type="number" name="name" value="1" min="1" class="form-control">
                            <a href="#" class="vk-btn _plus" data-index="plus">+</a>
                            <a href="#" class="vk-btn _minus" data-index="minus">-</a>
                        </div> <!--./calculator-->
                    </td>
                    <td>
                        <div class="vk-shopcart-item__price--box">
                            <span class="vk-shopcart-item__price--total">1.000.000</span> đ
                        </div>
                    </td>

                </tr>

                <tr>
                    <td>1</td>
                    <td>
                        <div class="vk-shopcart-item">
                            <a href="#" class="vk-shopcart-item__img">
                                <img src="images/cart-1.jpg" alt="">
                            </a>
                            <div class="vk-shopcart-item__brief">
                                <h3 class="vk-shopcart-item__title"><a href="#">Tiêu đề mẫu</a></h3>
                                <div class="vk-shopcart-item__text">
                                    <p>Màu sắc: <span class="_color"><img src="" alt=""></span></p>
                                    <p>Size: <span class="badge badge-secondary">XS</span></p>
                                </div>
                                <a href="#" class="vk-shopcart-item__btn-del"><i class="_icon fa fa-trash"></i> Xóa sản phẩm</a>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="vk-shopcart-item__price" data-price="1000000">1.000.000 đ</div>
                    </td>
                    <td>
                        <div class="vk-calculator" data-calculator-cart>
                            <input type="number" name="name" value="1" min="1" class="form-control">
                            <a href="#" class="vk-btn _plus" data-index="plus">+</a>
                            <a href="#" class="vk-btn _minus" data-index="minus">-</a>
                        </div> <!--./calculator-->
                    </td>
                    <td>
                        <div class="vk-shopcart-item__price--box">
                            <span class="vk-shopcart-item__price--total">1.000.000</span> đ
                        </div>

                    </td>

                </tr>
                <tr>
                    <td>1</td>
                    <td>
                        <div class="vk-shopcart-item">
                            <a href="#" class="vk-shopcart-item__img">
                                <img src="images/cart-1.jpg" alt="">
                            </a>
                            <div class="vk-shopcart-item__brief">
                                <h3 class="vk-shopcart-item__title"><a href="#">Tiêu đề mẫu</a></h3>
                                <div class="vk-shopcart-item__text">
                                    <p>Màu sắc: <span class="_color"><img src="" alt=""></span></p>
                                    <p>Size: <span class="badge badge-secondary">XS</span></p>
                                </div>
                                <a href="#" class="vk-shopcart-item__btn-del"><i class="_icon fa fa-trash"></i> Xóa sản phẩm</a>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="vk-shopcart-item__price" data-price="1000000">1.000.000 đ</div>
                    </td>
                    <td>
                        <div class="vk-calculator" data-calculator-cart>
                            <input type="number" name="name" value="1" min="1" class="form-control">
                            <a href="#" class="vk-btn _plus" data-index="plus">+</a>
                            <a href="#" class="vk-btn _minus" data-index="minus">-</a>
                        </div> <!--./calculator-->
                    </td>
                    <td>
                        <div class="vk-shopcart-item__price--box">
                            <span class="vk-shopcart-item__price--total">1.000.000</span> đ
                        </div>
                    </td>

                </tr>
                </tbody>

            </table>
        </div>

        <div class="vk-shopcart__total">
            Thành tiền: <b id="shopcartPriceTotal">57.000.000</b> <b>đ</b>
        </div>

        <div class="vk-shopcart__bot">
            <div class="_left">
                <a href="shop.php" class="vk-shopcart__btn-continue"><i class="_icon fa fa-angle-double-right"></i>Tiếp tục mua hàng</a>
                <a href="#" class="vk-shopcart__btn-removeAll">Xóa giỏ hàng</a>
            </div>
            <div class="_right">
                <a href="checkout.php" class="vk-shopcart__btn-checkout">Thanh toán</a>
            </div>
        </div>

    </div>
</div>

<?php
//Footer
$footer->render();

//srcipt
include 'template/modules/end.temp.php';