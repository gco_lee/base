<?php
include 'classes/classes.php';

//html
$head->render('Tin tức');
$header->class_header = '';
$header->render();

$breadcrumb->render(['Tin tức']);
?>
    <div class="container">
        <h1 class="vk-blog__heading">Tin tức</h1>
        <div class="vk-blog__list row">
            <?php $listItem->blog_item(); ?>
        </div>
        <?php $pagination->render(); ?>
    </div>

<?php
//Footer
$footer->render();


//srcipt
include 'template/modules/end.temp.php';


